"use strict";
//Feature 3

const address = document.getElementById("address");
const roomNumber = document.getElementById("roomNumber");
const lights = document.getElementById("lights");
const heatingCooling = document.getElementById("heatingCooling");
const seatsTotal = document.getElementById("seatsTotal");
const seatsUsed = document.getElementById("seatsUsed");
var newRoomList = new RoomUsageList();

function SAVE()
    {
        //Conditions for every inputs
        if (address.value==="")
            {
                displayMessage('Enter a valid address',3000);
            }
    
        if (roomNumber.value==="")
            {
                displayMessage('Enter a room number',3000);
            }
    
        if (seatsTotal.value==="")
            {
                displayMessage('Enter the total number of seats',3000);
            }
    
        if (seatsUsed.value==="")
            {
                seatsUsed.value=0
            }else if (seatsUsed.value>seatsTotal.value)
                {
                    displayMessage('Enter the appropriate number of seats used',3000)
                }
        
    //Feature 5
        var saveddata = new RoomUsage(roomNumber.value, address.value, lights.checked, heatingCooling.checked, seatsUsed.value, seatsTotal.value);
        var arr = newRoomList._roomList;
        arr.push(newRoomList.newRoom(saveddata));
        newRoomList.newRoom(saveddata);
        localStorage.setItem('ENG1003-RoomUseList',JSON.stringify(newRoomList));
 
    }

function clearroom()
    {
        location.reload();
    }

//Feature 4



//Feature 6

let arr;
function retrieveRoomList() {
    if (typeof (Storage) !== "undefined") {
        arr = JSON.parse(localStorage.getItem('ENG1003-RoomUseList'))
        console.log(arr)
    } else {
        console.log("Error: localStorage is not supported by current browser.");
    }
    return arr;
}
