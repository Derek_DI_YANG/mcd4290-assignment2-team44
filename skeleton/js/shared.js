    "use strict";
//Feature 1
class RoomUsage {
        constructor(roomNumber,address,lightsOn,heatingCoolingOn,seatsUsed,seatsTotal,timeChecked) {
            this._roomNumber = roomNumber;
            this._address = address; //Building address
            this._lightsOn = lightsOn;
            this._heatingCoolingOn = heatingCoolingOn; //Air Conditioning/Heating on
            this._seatsUsed = seatsUsed; //Number of seats in use
            this._seatsTotal = seatsTotal; //Total number of seats in room
            let now = new Date(); //Current date and time
            this._timeChecked = now; //Date/time usage checked
        }
    get roomNumber(){
        return this._roomNumber;
      }
    set roomNumber(newRoomNumber) {
        this._roomNumber = newRoomNumber;
    }
        
    get address() {
        return this._address;
    }
    set address(newAddress) {
            this._address = newAddress;
    }
        
    get lightsOn() {
        return this._lightsOn;
    }    
    set lightsOn(newLightsOn) {
        if (typeof(newLightsOn) === "boolean")
        {
            this._lightsOn = newLightsOn;
        }
    }
        
    get heatingCoolingOn() {
        return this._heatingCoolingOn;
    }
    set heatingCooling(newHeatingCoolingOn) { 
        if (typeof(newHeatingCoolingOn) === "boolean")
        {
            this._heatingCoolingOn = newHeatingCoolingOn;
        }
    }
        
    get seatsTotal() {
        return this._seatsTotal;
    }
    set seatsTotal(newseatsTotal)
        {
            if ((newseatsTotal) > 0 )
            {
                this._seatsTotal = newseatsTotal;
            }
        }

    get seatsUsed() {
        return this._seatsUsed;
    }
    set seatsUsed(newseatsUsed)
        {
            if ((newseatsUsed) <= this._seatsTotal )
            {
                this._seatsUsed = newseatsUsed;
            }
        }
    }

//Feature 2
    class RoomUsageList{
        constructor(){
            this._roomList = [];
        }

        get roomList () {
            return this._roomList;
        }

         set roomList (newRoomList) {
            this._roomList = newRoomList;
        }

        newRoom(RoomObservation){
            this._roomList.push(RoomObservation)
        }
    }